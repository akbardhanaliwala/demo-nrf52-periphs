#define _MAIN_C_SRC

//-------------------------MODULES USED-------------------------------------
#include "unity.h"
#include "ledctrl.h"
#include "test_ledctrl.h"
//-------------------------DEFINITIONS AND MACORS---------------------------



//-------------------------TYPEDEFS AND STRUCTURES--------------------------



//-------------------------PROTOTYPES OF LOCAL FUNCTIONS--------------------



//-------------------------EXPORTED VARIABLES ------------------------------



//-------------------------GLOBAL VARIABLES---------------------------------



//-------------------------EXPORTED FUNCTIONS-------------------------------
void setUp(void)
{
    ledctrl_init();
}

void tearDown(void)
{

}

int main(void)
{

    UnityBegin("modules/ledctrl.c");
    DO_TEST(test_check_led_num);
    DO_TEST(test_returns_true_for_active);
    DO_TEST(test_rerturns_false_for_invalid_led_num);
    UnityEnd();

    return 0;
}

void HardFault_Handler(void)
{
    while(1);
}
//-------------------------LOCAL FUNCTIONS----------------------------------
