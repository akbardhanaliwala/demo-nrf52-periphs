#define _TEST_EXAMPLE_C_SRC

//-------------------------MODULES USED-------------------------------------
#include "unity.h"
#include "nrf_delay.h"
#include "ledctrl.h"
#include "test_ledctrl.h"

//-------------------------DEFINITIONS AND MACORS---------------------------



//-------------------------TYPEDEFS AND STRUCTURES--------------------------



//-------------------------PROTOTYPES OF LOCAL FUNCTIONS--------------------



//-------------------------EXPORTED VARIABLES ------------------------------



//-------------------------GLOBAL VARIABLES---------------------------------



//-------------------------EXPORTED FUNCTIONS-------------------------------
void test_check_led_num(void)
{
	int i=0;
	for(i=0;i<MAX_LED_NUM;i++){
		TEST_ASSERT_EQUAL_INT(LEDCTRL_OK, ledctrl_onoff(true, i));
		TEST_ASSERT_EQUAL_INT(LEDCTRL_OK, ledctrl_onoff(false, i));
	}
	i=4;
	TEST_ASSERT_EQUAL_INT(LEDCTRL_ERR, ledctrl_onoff(true, i));
	TEST_ASSERT_EQUAL_INT(LEDCTRL_ERR, ledctrl_onoff(false, i));

}

void test_returns_true_for_active(void)
{
	int i=0;
	for(i=0;i<MAX_LED_NUM;i++){
		ledctrl_onoff(true, i);
		TEST_ASSERT_TRUE(ledctrl_isActive(i));
		ledctrl_onoff(false, i);
		TEST_ASSERT_FALSE(ledctrl_isActive(i));
	}
}

void test_rerturns_false_for_invalid_led_num(void)
{
	ledctrl_onoff(true, MAX_LED_NUM);
	TEST_ASSERT_FALSE(ledctrl_isActive(MAX_LED_NUM));

	ledctrl_onoff(true, MAX_LED_NUM+1);
	TEST_ASSERT_FALSE(ledctrl_isActive(MAX_LED_NUM+1));	
}

//-------------------------LOCAL FUNCTIONS----------------------------------

